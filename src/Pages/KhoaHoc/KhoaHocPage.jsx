import React, { useEffect, useState } from "react";
import Lottie from "lottie-react";
import course from "./course.json";
import { Pagination, Typography, Avatar } from "antd";
const { Paragraph, Text } = Typography;
import {
  FieldTimeOutlined,
  CalendarOutlined,
  BarChartOutlined,
} from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { LayKhoaHocTheoTrangUser } from "../../redux/QuanLyKhoaHocSlice";
import { useNavigate } from "react-router-dom";
const KhoaHocPage = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { dsKHTheoTrang } = useSelector((state) => {
    return state.QuanLyKhoaHocSlice;
  });
  const [current, setCurrent] = useState(1);
  const onChange = (page) => {
    setCurrent(page);
  };
  useEffect(() => {
    dispatch(LayKhoaHocTheoTrangUser(current));
  }, [current]);
  const { items } = dsKHTheoTrang;
  const cssKhoaHocPage = `
    xl:flex xl:justify-center xl:items-center 
    lg:flex lg:justify-center lg:items-center 
    md:flex md:justify-center md:items-center 
    sm:flex sm:justify-center sm:items-center 
    min-[360px]:block
  `;
  return (
    <div>
      <div className={cssKhoaHocPage}>
        <div className="w-3/5 xl:ml-5 lg:ml-5 md:ml-5 sm:ml-5 text-center min-[360px]:w-full min-[360px]:pt-20">
          <h1 className="xl:text-4xl lg:text-4xl md:text-3xl sm:text-2xl text-yellow-600 font-medium min-[360px]:text-3xl">
            CHỌN KHÓA HỌC THEO SỞ THÍCH CỦA CÁC BẠN. <br /> CHỨ KHÔNG HỌC CHO
            NGƯỜI KHÁC.
          </h1>
          <h1 className="animated-text xl:text-3xl lg:text-3xl md:text-2xl sm:text-xl min-[360px]:text-2xl text-green-600 font-medium">
            BẮT ĐẦU HÀNH TRÌNH HỌC THÔI NÀO
          </h1>
        </div>
        <div className="w-2/5 xl:h-screen lg:h-screen md:h-screen sm:h-screen lg:flex lg:items-center md:flex md:items-center sm:flex sm:items-center min-[360px]:w-full">
          <Lottie
            animationData={course}
            loop={true}
            width="100%"
            height="100%"
          />
        </div>
      </div>
      <div className="pt-20 mx-9 grid xl:grid-cols-6 lg:grid-cols-6 md:grid-cols-6 sm:grid-cols-3 min-[360px]:grid-cols-1">
        <div className="bg-yellow-500 text-center text-white py-5">
          <h1 className="text-lg font-medium">CHƯƠNG TRÌNH HỌC</h1>
          <i
            className="fas fa-laptop"
            style={{ fontSize: 30, padding: 10 }}
          ></i>
          <h1 className="text-xl font-medium">300</h1>
        </div>
        <div className="bg-yellow-800 text-center text-white py-5">
          <h1 className="text-lg font-medium">NHÀ SÁNG TẠO</h1>
          <i
            className="fas fa-camera"
            style={{ fontSize: 30, padding: 10 }}
          ></i>
          <h1 className="text-xl font-medium">10000</h1>
        </div>
        <div className="bg-green-600 text-center text-white py-5">
          <h1 className="text-lg font-medium">NHÀ THIẾT KẾ</h1>
          <i
            className="fas fa-briefcase"
            style={{ fontSize: 30, padding: 10 }}
          ></i>
          <h1 className="text-xl font-medium">400</h1>
        </div>
        <div className="bg-blue-400 text-center text-white py-5">
          <h1 className="text-lg font-medium">BÀI GIẢNG</h1>
          <i className="fas fa-book" style={{ fontSize: 30, padding: 10 }}></i>
          <h1 className="text-xl font-medium">3000</h1>
        </div>
        <div className="bg-slate-400 text-center text-white py-5">
          <h1 className="text-lg font-medium">VIDEO</h1>
          <i className="fas fa-video" style={{ fontSize: 30, padding: 10 }}></i>
          <h1 className="text-xl font-medium">40000</h1>
        </div>
        <div className="bg-black text-center text-white py-5">
          <h1 className="text-lg font-medium">LĨNH VỰC</h1>
          <i
            className="fas fa-sitemap"
            style={{ fontSize: 30, padding: 10 }}
          ></i>
          <h1 className="text-xl font-medium">200</h1>
        </div>
      </div>
      <div className="container py-14">
        <h1 className="text-2xl font-medium text-green-600">
          DANH SÁCH KHÓA HỌC
        </h1>
        <div className="py-10">
          <div className="bg-white">
            <div className="mx-auto max-w-2xl sm:px-6 sm:py-15 lg:py-10 lg:max-w-7xl">
              <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
                {items?.map((item, index) => (
                  <div
                    key={index}
                    onClick={() => {
                      navigate(`/thongTinKhoaHoc/${item.maKhoaHoc}`);
                    }}
                    className="group relative border shadow-md cursor-pointer transition delay-200 rounded-lg hover:-translate-y-2"
                  >
                    <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none lg:h-64">
                      <img
                        src="https://www.altamira.ai/wp-content/uploads/2022/12/Full-Stack-DeveloperArtboard-2.png"
                        alt=""
                        className="w-full h-full object-cover object-center lg:h-full lg:w-full"
                      />
                    </div>
                    <div className="p-4">
                      <Text
                        className="text-yellow-600 py-3 text-2xl font-medium"
                        style={{
                          width: 80,
                        }}
                      >
                        {item.tenKhoaHoc}
                      </Text>
                      <Paragraph
                        className="text-gray-500 text-xl"
                        ellipsis={{
                          rows: 2,
                        }}
                      >
                        {item.moTa}
                      </Paragraph>
                      <div className="flex pb-5">
                        <div className="w-2/6 text-center">
                          <p>
                            <FieldTimeOutlined className="text-yellow-600 sm:text-xl xl:text-2xl lg:text-2xl md:text-2xl min-[360px]:text-2xl" />
                            <span className="pl-2 font-medium">8 Giờ</span>
                          </p>
                        </div>
                        <div className="w-2/6 text-center">
                          <p>
                            <CalendarOutlined className="text-red-500 sm:text-xl xl:text-2xl lg:text-2xl md:text-2xl min-[360px]:text-2xl" />
                            <span className="pl-2 font-medium">4 Tuần</span>
                          </p>
                        </div>
                        <div className="w-2/6 text-center">
                          <p>
                            <BarChartOutlined className="text-blue-400 sm:text-xl xl:text-2xl lg:text-2xl md:text-2xl min-[360px]:text-2xl" />
                            <span className="pl-2 sm: font-medium">Tất Cả</span>
                          </p>
                        </div>
                      </div>
                      <hr />
                      <div className="flex justify-center items-center">
                        <div className="w-3/5 pt-4">
                          <div className="flex pb-5">
                            <div className="w-1/5">
                              <Avatar src="https://i.pravatar.cc/200?u=fake@pravatar.com "></Avatar>
                            </div>
                            <div className="w-4/5">
                              <p className="uppercase text-sm font-medium pl-3 min-[360px]:text-lg">
                                {item.nguoiTao.hoTen}
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="w-2/5">
                          <p className="line-through text-xs text-center">
                            800.000đ
                          </p>
                          <h1 className="color-changing-text sm:text-xl min-[360px]:text-center min-[360px]:text-2xl">
                            Miễn Phí
                          </h1>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="text-center text-2xl">
          <Pagination current={current} onChange={onChange} total={50} />
        </div>
      </div>
    </div>
  );
};

export default KhoaHocPage;
