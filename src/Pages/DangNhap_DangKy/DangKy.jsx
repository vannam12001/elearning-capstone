import React from "react";
import { useNavigate } from "react-router-dom";
import { Form, Input, Select, message } from "antd";
import Lottie from "lottie-react";
import animateRegister from "./register.json";
import { LayTaiKhoanDangKy } from "../../redux/QuanLyNguoiDungSlice";
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};
const DangKy = () => {
  const [form] = Form.useForm();
  let navigate = useNavigate();
  const onFinish = (values) => {
    console.log("Success:", values);
    LayTaiKhoanDangKy(values)
      .then((res) => {
        console.log(res);
        if (res.status !== 200) throw new Error(`status: ${res}`);
        message.success("Đăng ký thành công!!!");
        setTimeout(() => {
          navigate("/dang-nhap");
        }, 2000);
      })
      .catch((err) => {
        console.log(err);
        message.error(err.response.data);
      });
  };
  const cssDangKy = `
    xl:flex xl:justify-center xl:items-center 
    lg:flex lg:justify-center lg:items-center 
    md:flex md:justify-center md:items-center 
    sm:block
    min-[360px]:block
  `;
  const cssItemLeft = `
    xl:w-1/2 xl:h-screen 
    lg:w-1/2 lg:h-screen 
    md:w-[60%] md:pt-14
    sm:w-full sm:pt-20
    min-[360px]:w-full min-[360px]:pt-20 min-[360px]:container
  `;
  const cssItemRight = `
    xl:w-1/2 xl:h-screen 
    lg:w-1/2 lg:h-screen lg:flex lg:items-center 
    md:w-[40%] md:h-screen md:flex md:items-center
    sm:w-full
  `;
  return (
    <div className={cssDangKy}>
      <div className={cssItemLeft}>
        <h1 className="text-center pb-5 xl:text-4xl lg:text-4xl md:text-2xl text-green-600 min-[360px]:text-3xl font-medium">
          CHÀO MỪNG BẠN ĐẾN VỚI
          <br />
          CYPERLEARNING
        </h1>
        <Form
          {...formItemLayout}
          form={form}
          name="register"
          onFinish={onFinish}
          initialValues={{
            residence: ["zhejiang", "hangzhou", "xihu"],
            prefix: "86",
          }}
          style={{
            maxWidth: 600,
          }}
          scrollToFirstError
        >
          <Form.Item
            name="taiKhoan"
            label="Tài Khoản"
            tooltip="Bạn muốn được gọi là gì?"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên tài khoản!!!",
              },
              {
                type: "string",
                pattern: /^\S+$/,
                message: "Tên tài khoản không được có khoảng cách!!!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="hoTen"
            label="Họ Tên"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên của bạn!!!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="soDt"
            label="SDT"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập số điện thoại của bạn!!!",
              },
              {
                pattern: /^(0\d{9}|84\d{9})$/,
                message: "Vui lòng nhập đúng số điện thoại của bạn!!!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="email"
            label="E-mail"
            rules={[
              {
                type: "email",
                message: "Bạn nhập không đúng định dạng email!!!",
              },
              {
                required: true,
                message: "Vui lòng nhập email của bạn!!!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="matKhau"
            label="Mật Khẩu"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mật khẩu của bạn!!!",
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={["matKhau"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Vui lòng nhập lại mật khẩu của bạn!!!",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("matKhau") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error(
                      "Mật khẩu bạn mới nhập không có giống với mất khẩu của bạn!!!"
                    )
                  );
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            name="maNhom"
            label="Mã Nhóm"
            rules={[
              {
                required: true,
                message: "Vui lòng chọn mã nhóm!!!",
              },
            ]}
          >
            <Select>
              <Select.Option value="GP01">GP01</Select.Option>
              <Select.Option value="GP02">GP02</Select.Option>
              <Select.Option value="GP03">GP03</Select.Option>
              <Select.Option value="GP04">GP04</Select.Option>
              <Select.Option value="GP05">GP05</Select.Option>
              <Select.Option value="GP06">GP06</Select.Option>
              <Select.Option value="GP07">GP07</Select.Option>
              <Select.Option value="GP08">GP08</Select.Option>
              <Select.Option value="GP09">GP09</Select.Option>
              <Select.Option value="GP10">GP10</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <button
              className="bg-yellow-600 py-2 w-full rounded-xl text-2xl font-medium transition delay-300 hover:text-white"
              type="submit"
            >
              ĐĂNG KÝ
            </button>
          </Form.Item>
        </Form>
      </div>
      <div className={cssItemRight}>
        <Lottie
          animationData={animateRegister}
          className="justify-center"
          loop={true}
          width="100%"
          height="100%"
        />
      </div>
    </div>
  );
};

export default DangKy;
