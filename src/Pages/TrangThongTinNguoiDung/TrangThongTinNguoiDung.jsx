import React, { useEffect, useRef, useState } from "react";
import Lottie from "lottie-react";
import user from "./animation_user.json";
import {
  DesktopOutlined,
  FieldTimeOutlined,
  CalendarOutlined,
  BarChartOutlined,
} from "@ant-design/icons";
import {
  CapNhatThongTinNguoiDung,
  LayThongTinTaiKhoan,
  setInfo,
  setLogin,
} from "../../redux/QuanLyNguoiDungSlice";
import { Avatar, Tabs, Modal, Form, Input, Select, message, Rate } from "antd";
import Draggable from "react-draggable";
import { useDispatch, useSelector } from "react-redux";
import { huyDangKyKhoaHoc } from "../../redux/QuanLyKhoaHocSlice";
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};
const TrangThongTinNguoiDung = () => {
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [bounds, setBounds] = useState({
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
  });
  const draggleRef = useRef(null);
  const showModal = () => {
    setOpen(true);
  };
  const handleOk = (e) => {
    setOpen(false);
  };
  const handleCancel = (e) => {
    setOpen(false);
  };
  const onStart = (_event, uiData) => {
    const { clientWidth, clientHeight } = window.document.documentElement;
    const targetRect = draggleRef.current?.getBoundingClientRect();
    if (!targetRect) {
      return;
    }
    setBounds({
      left: -targetRect.left + uiData.x,
      right: clientWidth - (targetRect.right - uiData.x),
      top: -targetRect.top + uiData.y,
      bottom: clientHeight - (targetRect.bottom - uiData.y),
    });
  };
  const [khoaHoc, setKhoaHoc] = useState();
  const fetchUserInfo = async () => {
    const result = await LayThongTinTaiKhoan();
    dispatch(setInfo(result.data));
    setKhoaHoc(result.data.chiTietKhoaHocGhiDanh);
  };
  useEffect(() => {
    fetchUserInfo();
  }, []);
  const onChange = (key) => {
    console.log(key);
  };
  const { userLogin, thongTinTaiKhoan } = useSelector((state) => {
    return state.QuanLyNguoiDungSlice;
  });
  const [form] = Form.useForm();
  const onFinish = (values) => {
    CapNhatThongTinNguoiDung(values)
      .then((res) => {
        dispatch(setLogin(res.data));
        if (res.status !== 200) throw new Error(`status: ${res}`);
        message.success("Cập Nhật Thành Công!!!");
      })
      .catch((err) => {
        console.log(err);
        message.error("Cập Nhật Thất Bại");
      });
  };
  const handleHuyKhoaHoc = () => {
    const params = {
      taiKhoan: userLogin.taiKhoan,
      maKhoaHoc: khoaHoc[0].maKhoaHoc,
    };
    const fetch = async () => {
      await huyDangKyKhoaHoc(params)
        .then((res) => {
          console.log(res);
          message.success("Hủy Khóa Học Thành Công");
          fetchUserInfo();
        })
        .catch((err) => {
          message.error("Hủy Thất Bại");
          console.log(err);
        });
    };
    fetch();
  };
  const items = [
    {
      key: "1",
      label: (
        <button className="text-xl py-2 px-3 border-yellow-600 border font-medium text-yellow-600 transition delay-200 hover:text-black">
          Thông Tin Cá Nhân
        </button>
      ),
      children: (
        <div className="flex">
          <div className="w-1/2 py-5 min-[360px]:mr-10">
            <h1 className="text-xl pb-4">
              <span className="font-medium pr-2">Họ và Tên:</span>{" "}
              {thongTinTaiKhoan.hoTen}
            </h1>
            <h1 className="text-xl pb-4">
              <span className="font-medium pr-2">Email:</span>{" "}
              {thongTinTaiKhoan.email}
            </h1>
            <h1 className="text-xl pb-4">
              <span className="font-medium pr-2">Số Điện Thoại:</span>{" "}
              {thongTinTaiKhoan.soDT}
            </h1>
          </div>
          <div className="w-1/2 py-5">
            <h1 className="text-xl pb-4">
              <span className="font-medium pr-2">Tài Khoản:</span>{" "}
              {thongTinTaiKhoan.taiKhoan}
            </h1>
            <h1 className="text-xl pb-4">
              <span className="font-medium pr-2">Nhóm:</span>{" "}
              {thongTinTaiKhoan.maNhom}
            </h1>
            <h1 className="text-xl pb-4">
              <span className="font-medium pr-2">Đối Tượng:</span>{" "}
              {thongTinTaiKhoan.maLoaiNguoiDung === "GV"
                ? "Giảng Viên"
                : "Học Viên"}
            </h1>
            <button
              className="bg-yellow-600 py-2 px-3 rounded-xl text-xl font-medium transition delay-200 hover:text-white"
              onClick={showModal}
            >
              Cập Nhật
            </button>
            <Modal
              title={
                <div
                  style={{
                    width: "100%",
                    cursor: "move",
                  }}
                  onMouseOver={() => {
                    if (disabled) {
                      setDisabled(false);
                    }
                  }}
                  onMouseOut={() => {
                    setDisabled(true);
                  }}
                  onFocus={() => {}}
                  onBlur={() => {}}
                >
                  <h1 className="text-2xl border-b-2 border-black pb-3">
                    Chỉnh Sửa Thông Tin Cá Nhân
                  </h1>
                </div>
              }
              open={open}
              onOk={handleOk}
              onCancel={handleCancel}
              modalRender={(modal) => (
                <Draggable
                  disabled={disabled}
                  bounds={bounds}
                  nodeRef={draggleRef}
                  onStart={(event, uiData) => onStart(event, uiData)}
                >
                  <div ref={draggleRef}>{modal}</div>
                </Draggable>
              )}
            >
              <Form
                {...formItemLayout}
                form={form}
                name="register"
                onFinish={onFinish}
                initialValues={{
                  residence: ["zhejiang", "hangzhou", "xihu"],
                  prefix: "86",
                }}
                style={{
                  maxWidth: 600,
                }}
                scrollToFirstError
              >
                <Form.Item
                  name="thongTinTaiKhoan"
                  label="Tài Khoản"
                  tooltip="Bạn muốn được gọi là gì?"
                  initialValue={userLogin.taiKhoan}
                >
                  <Input disabled />
                </Form.Item>

                <Form.Item
                  name="hoTen"
                  label="Họ Tên"
                  rules={[
                    {
                      required: true,
                      message: "Vui lòng nhập tên của bạn!!!",
                    },
                  ]}
                  initialValue={userLogin.hoTen}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="soDt"
                  label="SDT"
                  rules={[
                    {
                      required: true,
                      message: "Vui lòng nhập số điện thoại của bạn!!!",
                    },
                    {
                      pattern: /^(0\d{9}|84\d{9})$/,
                      message: "Vui lòng nhập đúng số điện thoại của bạn!!!",
                    },
                  ]}
                  initialValue={userLogin.soDT}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="email"
                  label="E-mail"
                  rules={[
                    {
                      type: "email",
                      message: "Bạn nhập không đúng định dạng email!!!",
                    },
                    {
                      required: true,
                      message: "Vui lòng nhập email của bạn!!!",
                    },
                  ]}
                  initialValue={userLogin.email}
                >
                  <Input />
                </Form.Item>

                <Form.Item
                  name="matKhau"
                  label="Mật Khẩu"
                  rules={[
                    {
                      required: true,
                      message: "Vui lòng nhập mật khẩu của bạn!!!",
                    },
                  ]}
                  hasFeedback
                >
                  <Input.Password />
                </Form.Item>
                <Form.Item
                  name="confirm"
                  label="Confirm Password"
                  dependencies={["matKhau"]}
                  hasFeedback
                  rules={[
                    {
                      required: true,
                      message: "Vui lòng nhập lại mật khẩu của bạn!!!",
                    },
                    ({ getFieldValue }) => ({
                      validator(_, value) {
                        if (!value || getFieldValue("matKhau") === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject(
                          new Error(
                            "Mật khẩu bạn mới nhập không có giống với mất khẩu của bạn!!!"
                          )
                        );
                      },
                    }),
                  ]}
                >
                  <Input.Password />
                </Form.Item>
                <Form.Item
                  name="maNhom"
                  label="Mã Nhóm"
                  rules={[
                    {
                      required: true,
                      message: "Vui lòng chọn mã nhóm!!!",
                    },
                  ]}
                >
                  <Select>
                    <Select.Option value="GP01">GP01</Select.Option>
                    <Select.Option value="GP02">GP02</Select.Option>
                    <Select.Option value="GP03">GP03</Select.Option>
                    <Select.Option value="GP04">GP04</Select.Option>
                    <Select.Option value="GP05">GP05</Select.Option>
                    <Select.Option value="GP06">GP06</Select.Option>
                    <Select.Option value="GP07">GP07</Select.Option>
                    <Select.Option value="GP08">GP08</Select.Option>
                    <Select.Option value="GP09">GP09</Select.Option>
                    <Select.Option value="GP10">GP10</Select.Option>
                  </Select>
                </Form.Item>
                <Form.Item
                  name="maLoaiNguoiDung"
                  label="Mã Loại Người Dùng"
                  rules={[
                    {
                      required: true,
                      message: "Vui lòng chọn mã loại người dùng!!!",
                    },
                  ]}
                >
                  <Select>
                    <Select.Option value="GV">GV</Select.Option>
                    <Select.Option value="HV">HV</Select.Option>
                  </Select>
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                  <button
                    className="bg-yellow-600 py-2 w-full rounded-xl text-2xl font-medium transition delay-300 hover:text-white"
                    type="submit"
                  >
                    HOÀN THÀNH
                  </button>
                </Form.Item>
              </Form>
            </Modal>
          </div>
        </div>
      ),
    },
    {
      key: "2",
      label: (
        <button className="text-xl py-2 px-3 font-medium border-yellow-600 border text-yellow-600 transition delay-200 hover:text-black">
          Khóa Học Của Tôi
        </button>
      ),
      children: (
        <div>
          <h1 className="py-5 text-3xl font-medium">Khóa Học Của Tôi</h1>
          {khoaHoc !== null ? (
            <div className="h-[500px] overflow-y-scroll">
              {khoaHoc?.map((item, index) => {
                return (
                  <div
                    key={index}
                    className="xl:flex lg:flex md:flex sm:flex min-[360px]:block min-[360px]:mx-4 pb-8"
                  >
                    <div className="xl:w-[30%] lg:w-[30%] md:w-[30%] sm:w-[30%] min-[360px]:w-full">
                      <img
                        src="https://www.altamira.ai/wp-content/uploads/2022/12/Full-Stack-DeveloperArtboard-2.png"
                        width={300}
                        style={{ height: 200 }}
                        alt=""
                      />
                    </div>
                    <div className="xl:w-[70%] lg:w-[70%] md:w-[70%] sm:w-[70%] min-[360px]:w-full">
                      <h1 className="text-2xl font-medium pb-2">
                        {item.tenKhoaHoc}
                      </h1>
                      <h1 className="text-xl text-gray-500 font-medium pb-2">
                        Full Stack Developer là những người có nhiệm vụ tổng hợp
                        kiến thức và sự hiểu biết trực quan, sâu sắc về Front
                        End và Back End.
                      </h1>
                      <div className="flex pb-2">
                        <div className="w-1/3 text-center">
                          <p>
                            <FieldTimeOutlined
                              style={{ fontSize: 20 }}
                              className="text-yellow-600"
                            />
                            <span className="pl-2 font-medium">8 Giờ</span>
                          </p>
                        </div>
                        <div className="w-1/3 text-center">
                          <p>
                            <CalendarOutlined
                              className="text-red-500"
                              style={{ fontSize: 20 }}
                            />
                            <span className="pl-2 font-medium">4 Tuần</span>
                          </p>
                        </div>
                        <div className="w-1/3 text-center">
                          <p>
                            <BarChartOutlined
                              className="text-blue-400"
                              style={{ fontSize: 20 }}
                            />
                            <span className="pl-2 font-medium">Tất Cả</span>
                          </p>
                        </div>
                      </div>
                      <Rate allowHalf defaultValue={5} />
                      <div className="flex">
                        <div className="w-1/2">
                          <Avatar src="https://i.pravatar.cc/?u=b" size={50} />
                          <span className="color-changing-text text-xl">
                            Trần Quang Sĩ
                          </span>
                        </div>
                        <div className="w-1/2 text-right">
                          <button
                            className="bg-yellow-600 rounded-xl font-medium py-2 px-3 text-xl transition delay-200 hover:-translate-y-2"
                            onClick={() => {
                              handleHuyKhoaHoc();
                            }}
                          >
                            Hủy Khóa Học
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          ) : (
            <></>
          )}
        </div>
      ),
    },
  ];
  return (
    <div>
      <div className="xl:flex lg:flex md:flex sm:block min-[360px]:block justify-center items-center">
        <div className="xl:w-1/2 lg:w-1/2 md:w-1/2 sm:block sm:pt-24 min-[360px]:block min-[360px]:pt-24 text-center">
          <h1 className="xl:text-5xl lg:text-5xl md:text-4xl sm:text-4xl min-[360px]:text-3xl text-yellow-600 font-medium">
            THÔNG TIN CÁ NHÂN
          </h1>
          <h1 className="text-3xl font-medium pt-5">Thông Tin Học Viên</h1>
        </div>
        <div
          className="
        xl:w-1/2 lg:w-1/2 md:w-1/2 
        md:flex sm:block min-[360px]:block 
        items-center xl:h-screen lg:h-screen 
        md:h-screen pt-5"
        >
          <Lottie animationData={user} loop={true} width="100%" height="100%" />
        </div>
      </div>
      <div className="container xl:flex lg:flex md:flex sm:block min-[360px]:block py-14">
        <div className="xl:w-[20%] lg:w-[20%] md:w-[20%] sm:block min-[360px]:block md:mr-6 text-center">
          <Avatar src="https://i.pravatar.cc/?u=b" size={150} />
          <h1 className="text-xl font-medium py-3">{thongTinTaiKhoan.hoTen}</h1>
          {thongTinTaiKhoan.maLoaiNguoiDung === "GV" ? (
            <p>Admin CyperLearning</p>
          ) : (
            <p>Học Viên CyperLearning</p>
          )}
        </div>
        <div className="xl:w-[80%] lg:w-[80%] md:w-[80%] sm:block min-[360px]:block">
          <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
        </div>
      </div>
    </div>
  );
};

export default TrangThongTinNguoiDung;
