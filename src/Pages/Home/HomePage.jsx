import React from "react";
import Banner from "../../Components/Banner/Banner";
import InfoCources from "../../Components/InfoCources/InfoCources";
import Cources from "../../Components/Cources/Cources";
import Counter from "../../Components/Counter/Counter";
import TopTeacher from "../../Components/TopTeacher/TopTeacher";
import Review from "../../Components/Review/Review";

const HomePage = () => {
  return (
    <>
      <Banner />
      <InfoCources />
      <Cources />
      <Counter />
      <TopTeacher />
      <Review />
    </>
  );
};

export default HomePage;
