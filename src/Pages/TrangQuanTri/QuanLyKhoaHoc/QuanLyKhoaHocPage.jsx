import React, { useEffect, useState } from "react";
import { Pagination, Input, Modal, message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  DeleteKhoaHoc,
  LayDanhSachKhoaHocTheoTen,
  LayKhoaHocTheoTrangAdmin,
  thongTinKhoaHocDeCapNhat,
} from "../../../redux/QuanLyKhoaHocSlice";
import FormThemKhoaHoc from "./FormThemKhoaHoc";
import FormSuaKhoaHoc from "./FormSuaKhoaHoc";

const QuanLyKhoaHocPage = () => {
  const [showModal, setShowModal] = React.useState(false);
  const [open, setOpen] = useState(false);
  const [current, setCurrent] = useState(1);
  const onChange = (page) => {
    setCurrent(page);
  };
  const dispatch = useDispatch();
  const hanleOnErrorImage = (e) => {
    e.target.src =
      "https://www.altamira.ai/wp-content/uploads/2022/12/Full-Stack-DeveloperArtboard-2.png";
  };
  useEffect(() => {
    dispatch(LayKhoaHocTheoTrangAdmin(current));
  }, [current]);
  const { dsKHTheoTrang } = useSelector((state) => {
    return state.QuanLyKhoaHocSlice;
  });
  const { items } = dsKHTheoTrang;
  const handleDeleteKhoaHoc = (maKhoaHoc) => {
    DeleteKhoaHoc(maKhoaHoc)
      .then((res) => {
        message.success("Xóa tài khoản thành công!!!");
        dispatch(LayKhoaHocTheoTrangAdmin());
      })
      .catch((err) => {
        message.error(err.response.data);
      });
  };
  const handleTimKiem = (e) => {
    if (e.target.value === null) return;
    const fetch = () => {
      dispatch(LayDanhSachKhoaHocTheoTen(current, e.target.value));
    };
    fetch();
  };
  return (
    <>
      <div className="container flex pb-10">
        <div className="w-1/2">
          <button
            className="text-2xl font-medium rounded-lg bg-green-600 py-3 px-4 transition delay-200 hover:text-white"
            onClick={() => {
              setOpen(true);
            }}
          >
            Thêm Khóa Học
          </button>
          <Modal
            title={<h1>THÔNG TIN KHÓA HỌC</h1>}
            centered
            open={open}
            onOk={() => setOpen(false)}
            onCancel={() => setOpen(false)}
            width={800}
            footer={false}
          >
            <FormThemKhoaHoc />
          </Modal>
        </div>
        <div className="w-1/2">
          <Input
            placeholder="Nhập tên khóa học bạn muốn tìm kiếm"
            onChange={handleTimKiem}
          />
        </div>
      </div>
      <div>
        <table className="border border-black w-full">
          <tr className="text-base text-center">
            <th className="w-[7%]">STT</th>
            <th className="w-[15%]">Mã Khóa Học</th>
            <th className="w-[15%]">Tên Khóa Học</th>
            <th className="w-[14%]">Hình Ảnh</th>
            <th className="w-[11%]">Lượt Xem</th>
            <th className="w-[13%]">Người Tạo</th>
            <th className="w-[25%]">Setting</th>
          </tr>
          <tbody>
            {items?.map((item, index) => {
              return (
                <tr
                  key={index}
                  className="w-full h-[70px] border border-black text-base text-center cursor-pointer transition delay-200 hover:bg-slate-300"
                >
                  <td className="w-[5%]">{index + 1}</td>
                  <td className="w-[15%]">{item.maKhoaHoc}</td>
                  <td className="w-[15%]">{item.tenKhoaHoc}</td>
                  <td className="w-[14%]">
                    <img
                      src={item.hinhAnh}
                      alt=""
                      width="100%"
                      height={100}
                      onError={hanleOnErrorImage}
                    />
                  </td>
                  <td className="w-[11%]">{item.luotXem}</td>
                  <td className="w-[13%]">{item.nguoiTao.hoTen}</td>
                  <td className="w-[25%]">
                    <button className="bg-green-600 text-white py-2 px-3 rounded-lg mr-2">
                      Ghi Danh
                    </button>
                    <button
                      className="bg-yellow-600 py-2 px-3 rounded-lg mr-2"
                      onClick={() => {
                        console.log("Result", item);
                        setShowModal(true);
                        dispatch(thongTinKhoaHocDeCapNhat(item));
                      }}
                    >
                      Sửa
                    </button>
                    {showModal ? (
                      <>
                        <div className="justify-center pt-24 items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                          <div className="relative my-6 mx-auto w-[800px]">
                            <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                              <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                <h3 className="text-3xl font-medium">
                                  THÔNG TIN KHÓA HỌC
                                </h3>
                                <button
                                  className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                                  onClick={() => setShowModal(false)}
                                >
                                  <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                                    ×
                                  </span>
                                </button>
                              </div>
                              <div className="relative p-6 flex-auto">
                                <FormSuaKhoaHoc />
                                <div className="text-right">
                                  <button
                                    className="text-white bg-red-500 py-3 px-4 text-xl rounded"
                                    type="button"
                                    onClick={() => {
                                      setShowModal(false);
                                      dispatch(thongTinKhoaHocDeCapNhat(item));
                                    }}
                                  >
                                    Đóng
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                      </>
                    ) : null}
                    <button
                      className="bg-red-500 py-2 px-3 text-white rounded-lg"
                      onClick={() => {
                        handleDeleteKhoaHoc(item.maKhoaHoc);
                      }}
                    >
                      Xóa
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div className="text-center py-4">
        <Pagination
          current={current}
          onChange={onChange}
          total={dsKHTheoTrang.totalPages * 10}
        />
      </div>
    </>
  );
};

export default QuanLyKhoaHocPage;
