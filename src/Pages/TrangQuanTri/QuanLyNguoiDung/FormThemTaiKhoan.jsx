import { Form, Input, Select, message } from "antd";
import React from "react";
import {
  LayDanhSachNguoiDung,
  ThemTKNguoiDung,
} from "../../../redux/QuanLyNguoiDungSlice";
import { listMaNhomNguoiDung } from "../../../redux/QuanLyNguoiDungSlice";
import { useDispatch } from "react-redux";
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};
const FormThemTaiKhoan = () => {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const handleThemTKNguoiDung = (values) => {
    ThemTKNguoiDung(values)
      .then((res) => {
        console.log(res.data);
        if (res.status !== 200) throw new Error(`status: ${res}`);
        message.success("Thêm tài khoản người dùng thành công!!!");
        dispatch(LayDanhSachNguoiDung());
      })
      .catch((err) => {
        console.log(err);
        message.error("Thêm tài khoản thất bại!!!");
      });
  };
  return (
    <>
      <Form
        {...formItemLayout}
        form={form}
        name="register"
        onFinish={handleThemTKNguoiDung}
        initialValues={{
          residence: ["zhejiang", "hangzhou", "xihu"],
          prefix: "86",
        }}
        style={{
          maxWidth: 600,
        }}
        scrollToFirstError
      >
        <Form.Item
          name="taiKhoan"
          label="Tài Khoản"
          tooltip="Bạn muốn được gọi là gì?"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập tên tài khoản!!!",
            },
            {
              type: "string",
              pattern: /^\S+$/,
              message: "Tên tài khoản không được có khoảng cách!!!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="hoTen"
          label="Họ Tên"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập tên của bạn!!!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="soDt"
          label="SDT"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập số điện thoại của bạn!!!",
            },
            {
              pattern: /^(0\d{9}|84\d{9})$/,
              message: "Vui lòng nhập đúng số điện thoại của bạn!!!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="email"
          label="E-mail"
          rules={[
            {
              type: "email",
              message: "Bạn nhập không đúng định dạng email!!!",
            },
            {
              required: true,
              message: "Vui lòng nhập email của bạn!!!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="matKhau"
          label="Mật Khẩu"
          rules={[
            {
              required: true,
              message: "Vui lòng nhập mật khẩu của bạn!!!",
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="confirm"
          label="Confirm Password"
          dependencies={["matKhau"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Vui lòng nhập lại mật khẩu của bạn!!!",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("matKhau") === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error(
                    "Mật khẩu bạn mới nhập không có giống với mất khẩu của bạn!!!"
                  )
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="maNhom"
          label="Mã Nhóm"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn mã nhóm!!!",
            },
          ]}
        >
          <Select>
            {listMaNhomNguoiDung.map((item) => {
              return (
                <Select.Option key={item.id} value={item.maNhom}>
                  {item.maNhom}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item
          name="maLoaiNguoiDung"
          label="Mã Loại Người Dùng"
          rules={[
            {
              required: true,
              message: "Vui lòng chọn mã loại người dùng!!!",
            },
          ]}
        >
          <Select>
            <Select.Option value="HV">HV</Select.Option>
            <Select.Option value="GV">GV</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <button
            className="bg-yellow-600 py-2 w-full rounded-xl text-2xl font-medium transition delay-300 hover:text-white"
            type="submit"
          >
            THÊM NGƯỜI DÙNG
          </button>
        </Form.Item>
      </Form>
    </>
  );
};

export default FormThemTaiKhoan;
