import React from "react";
import { Form, Input, Select, message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  LayDanhSachNguoiDung,
  SuaTKNguoiDung,
  listMaNhomNguoiDung,
} from "../../../redux/QuanLyNguoiDungSlice";
const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};
const FormSuaTaiKhoan = () => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { thongTinSuaTaiKhoan } = useSelector((state) => {
    return state.QuanLyNguoiDungSlice;
  });
  const handleSuaThongTinNguoiDung = (values) => {
    console.log("Result", values);
    SuaTKNguoiDung(values)
      .then((res) => {
        console.log(res);
        if (res.status !== 200) throw new Error(`status: ${res}`);
        message.success("Sửa tài khoản người dùng thành công!!!");
        dispatch(LayDanhSachNguoiDung());
      })
      .catch((err) => {
        console.log(err);
        message.error("Sửa tài khoản người dùng thất bại");
      });
  };
  console.log(thongTinSuaTaiKhoan);
  return (
    <Form
      {...formItemLayout}
      form={form}
      name="edit"
      onFinish={handleSuaThongTinNguoiDung}
      initialValues={{
        residence: ["zhejiang", "hangzhou", "xihu"],
        prefix: "86",
      }}
      style={{
        maxWidth: 600,
      }}
      scrollToFirstError
    >
      <Form.Item
        name="taiKhoan"
        label="Tài Khoản"
        tooltip="Bạn muốn được gọi là gì?"
        rules={[
          {
            required: true,
            message: "Vui lòng nhập tên tài khoản!!!",
          },
          {
            type: "string",
            pattern: /^\S+$/,
            message: "Tên tài khoản không được có khoảng cách!!!",
          },
        ]}
        initialValue={thongTinSuaTaiKhoan.taiKhoan}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="hoTen"
        label="Họ Tên"
        rules={[
          {
            required: true,
            message: "Vui lòng nhập tên của bạn!!!",
          },
        ]}
        initialValue={thongTinSuaTaiKhoan.hoTen}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="soDt"
        label="SDT"
        rules={[
          {
            required: true,
            message: "Vui lòng nhập số điện thoại của bạn!!!",
          },
          {
            pattern: /^(0\d{9}|84\d{9})$/,
            message: "Vui lòng nhập đúng số điện thoại của bạn!!!",
          },
        ]}
        initialValue={thongTinSuaTaiKhoan.soDT}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="email"
        label="E-mail"
        rules={[
          {
            type: "email",
            message: "Bạn nhập không đúng định dạng email!!!",
          },
          {
            required: true,
            message: "Vui lòng nhập email của bạn!!!",
          },
        ]}
        initialValue={thongTinSuaTaiKhoan.email}
      >
        <Input />
      </Form.Item>

      <Form.Item
        name="matKhau"
        label="Mật Khẩu"
        rules={[
          {
            required: true,
            message: "Vui lòng nhập mật khẩu của bạn!!!",
          },
        ]}
        hasFeedback
      >
        <Input.Password />
      </Form.Item>
      <Form.Item
        name="maNhom"
        label="Mã Nhóm"
        rules={[
          {
            required: true,
            message: "Vui lòng chọn mã nhóm!!!",
          },
        ]}
      >
        <Select defaultValue={thongTinSuaTaiKhoan.maNhom}>
          {listMaNhomNguoiDung.map((item) => {
            return (
              <Select.Option key={item.id} value={item.maNhom}>
                {item.maNhom}
              </Select.Option>
            );
          })}
        </Select>
      </Form.Item>
      <Form.Item
        name="maLoaiNguoiDung"
        label="Mã Loại Người Dùng"
        rules={[
          {
            required: true,
            message: "Vui lòng chọn mã loại người dùng!!!",
          },
        ]}
      >
        <Select defaultValue={thongTinSuaTaiKhoan.maLoaiNguoiDung}>
          <Select.Option value="HV">HV</Select.Option>
          <Select.Option value="GV">GV</Select.Option>
        </Select>
      </Form.Item>
      <Form.Item {...tailFormItemLayout}>
        <button
          className="bg-yellow-600 py-2 w-full rounded-xl text-2xl font-medium transition delay-300 hover:text-white"
          type="submit"
        >
          CẬP NHẬT
        </button>
      </Form.Item>
    </Form>
  );
};

export default FormSuaTaiKhoan;
