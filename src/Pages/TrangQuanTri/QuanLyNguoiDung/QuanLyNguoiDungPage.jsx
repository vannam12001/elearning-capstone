import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  DeleteTaiKhoanNguoiDung,
  LayDanhSachNguoiDung,
  LayDanhSachNguoiDungTheoTen,
  thongTinTaiKhoanChonDeSua,
} from "../../../redux/QuanLyNguoiDungSlice";
import { Pagination, Input, message, Modal } from "antd";
import FormThemTaiKhoan from "./FormThemTaiKhoan";
import FormSuaTaiKhoan from "./FormSuaTaiKhoan";

const QuanLyNguoiDungPage = () => {
  const dispatch = useDispatch();
  const [current, setCurrent] = useState(1);
  const onChange = (page) => {
    setCurrent(page);
  };
  const [showModal, setShowModal] = React.useState(false);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  useEffect(() => {
    dispatch(LayDanhSachNguoiDung(current));
  }, [current]);

  const { listNguoiDungTheoTrang, userLogin, thongTinSuaTaiKhoan } =
    useSelector((state) => {
      return state.QuanLyNguoiDungSlice;
    });
  const { items } = listNguoiDungTheoTrang;
  const handleTimKiem = (e) => {
    if (e.target.value === null) return;
    const fetch = () => {
      dispatch(LayDanhSachNguoiDungTheoTen(current, e.target.value));
    };
    fetch();
  };
  const handleDeleteTaiKhoan = (taiKhoan) => {
    DeleteTaiKhoanNguoiDung(taiKhoan)
      .then((res) => {
        console.log(res);
        message.success("Xóa tài khoản thành công!!!");
        dispatch(LayDanhSachNguoiDung());
      })
      .catch((err) => {
        console.log(err);
        message.error(err.response.data);
      });
  };
  return (
    <>
      <div className="container flex pb-10">
        <div className="w-1/2">
          <button
            className="text-2xl font-medium rounded-lg bg-green-600 py-3 px-4 transition delay-200 hover:text-white"
            onClick={() => {
              setIsModalOpen(true);
            }}
          >
            Thêm Người Dùng
          </button>
          <Modal
            title={<h1>THÔNG TIN NGƯỜI DÙNG</h1>}
            open={isModalOpen}
            footer={false}
            maskClosable={true}
            onCancel={handleCancel}
            centered
          >
            <FormThemTaiKhoan />
          </Modal>
        </div>
        <div className="w-1/2">
          <Input
            placeholder="Nhập tài khoản hoặc họ tên bạn muốn tìm kiếm"
            onChange={handleTimKiem}
          />
        </div>
      </div>
      <div>
        <table className="border border-black w-full">
          <tr className="text-base text-center">
            <th className="w-[7%]">STT</th>
            <th className="w-[13%]">Tài Khoản</th>
            <th className="w-[7%]">Người Dùng</th>
            <th className="w-[15%]">Họ và Tên</th>
            <th className="w-[21%]">Email</th>
            <th className="w-[12%]">Số Điện Thoại</th>
            <th className="w-[25%]">Setting</th>
          </tr>
          <tbody>
            {items?.map((item, index) => {
              return (
                <tr
                  key={index}
                  className="w-full h-[70px] border border-black text-base text-center cursor-pointer transition delay-200 hover:bg-slate-300"
                >
                  <td className="w-[5%]">{index + 1}</td>
                  <td className="w-[13%]">{item.taiKhoan}</td>
                  <td className="w-[5%]">{item.maLoaiNguoiDung}</td>
                  <td className="w-[15%]">{item.hoTen}</td>
                  <td className="w-[25%]">{item.email}</td>
                  <td className="w-[10%]">{item.soDT}</td>
                  <td className="w-[27%]">
                    <button className="bg-green-600 text-white py-2 px-3 rounded-lg mr-2">
                      Ghi Danh
                    </button>
                    <button
                      className="bg-yellow-600 py-2 px-3 rounded-lg mr-2"
                      onClick={() => {
                        setShowModal(true);
                        dispatch(thongTinTaiKhoanChonDeSua(item));
                      }}
                    >
                      Sửa
                    </button>
                    {showModal ? (
                      <>
                        <div className="justify-center pt-28 items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                          <div className="relative my-6 mx-auto w-[500px]">
                            <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                              <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                                <h3 className="text-3xl font-medium">
                                  THÔNG TIN NGƯỜI DÙNG
                                </h3>
                                <button
                                  className="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                                  onClick={() => setShowModal(false)}
                                >
                                  <span className="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">
                                    ×
                                  </span>
                                </button>
                              </div>
                              <div className="relative p-6 flex-auto">
                                <FormSuaTaiKhoan />
                                <div className="text-right">
                                  <button
                                    className="text-white bg-red-500 py-3 px-4 text-xl rounded"
                                    type="button"
                                    onClick={() => {
                                      setShowModal(false);
                                      dispatch(thongTinTaiKhoanChonDeSua(null));
                                    }}
                                  >
                                    Đóng
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                      </>
                    ) : null}
                    <button
                      className="bg-red-500 py-2 px-3 text-white rounded-lg"
                      onClick={() => {
                        handleDeleteTaiKhoan(item.taiKhoan);
                      }}
                    >
                      Xóa
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div className="text-center py-4">
        <Pagination
          current={current}
          onChange={onChange}
          total={listNguoiDungTheoTrang.totalPages * 10}
        />
      </div>
    </>
  );
};

export default QuanLyNguoiDungPage;
