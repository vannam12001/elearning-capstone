import { createSlice } from "@reduxjs/toolkit";
import { localStorageServices } from "../Services/localStorageServices";
import { CYPERLEARN_LOCALSTORAGE, maNhom } from "../Constants";
import { Domain_Api } from "../Services/config";
//---------------state-----------------
const initialState = {
  userLogin: localStorageServices.getUser(CYPERLEARN_LOCALSTORAGE),
  thongTinTaiKhoan: {},
  listNguoiDungTheoTrang: {},
  thongTinSuaTaiKhoan: {},
};
export const listMaNhomNguoiDung = [
  {
    id: 1,
    maNhom: "GP01",
  },
  {
    id: 2,
    maNhom: "GP02",
  },
  {
    id: 3,
    maNhom: "GP04",
  },
  {
    id: 4,
    maNhom: "GP04",
  },
  {
    id: 5,
    maNhom: "GP05",
  },
  {
    id: 6,
    maNhom: "GP06",
  },
  {
    id: 7,
    maNhom: "GP07",
  },
  {
    id: 8,
    maNhom: "GP08",
  },
  {
    id: 9,
    maNhom: "GP09",
  },
  {
    id: 10,
    maNhom: "GP010",
  },
];
const QuanLyNguoiDungSlice = createSlice({
  name: CYPERLEARN_LOCALSTORAGE,
  initialState,
  reducers: {
    setLogin: (state, { type, payload }) => {
      state.userLogin = payload;
    },
    setInfo: (state, { type, payload }) => {
      state.thongTinTaiKhoan = payload;
    },
    thongTinTaiKhoanChonDeSua: (state, { type, payload }) => {
      state.thongTinSuaTaiKhoan = payload;
    },
    danhSachNguoiDungTheoTrang: (state, { type, payload }) => {
      state.listNguoiDungTheoTrang = payload;
    },
  },
});
export const {
  setLogin,
  setInfo,
  thongTinTaiKhoanChonDeSua,
  danhSachNguoiDungTheoTrang,
} = QuanLyNguoiDungSlice.actions;
//------------------------------------------

//-----------------api----------------------
export let LayTaiKhoanDangNhap = (values) => {
  return Domain_Api.post("/api/QuanLyNguoiDung/DangNhap", values);
};
export let LayTaiKhoanDangKy = (values) => {
  return Domain_Api.post("/api/QuanLyNguoiDung/DangKy", values);
};
export let LayThongTinTaiKhoan = () => {
  return Domain_Api.post("/api/QuanLyNguoiDung/ThongTinTaiKhoan");
};
export let CapNhatThongTinNguoiDung = (values) => {
  return Domain_Api.put("api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", values);
};
export const DeleteTaiKhoanNguoiDung = (taiKhoan) => {
  return Domain_Api.delete(
    `/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`
  );
};
export const ThemTKNguoiDung = (values) => {
  return Domain_Api.post(`/api/QuanLyNguoiDung/ThemNguoiDung`, values);
};
export const SuaTKNguoiDung = (values) => {
  return Domain_Api.put(
    "/api/QuanLyNguoiDung/CapNhatThongTinNguoiDung",
    values
  );
};
export const LayDanhSachNguoiDung = (currentPage = 1, value = "GP01") => {
  return async (dispatch) => {
    try {
      const { data } = await Domain_Api.get(
        `api/QuanLyNguoiDung/LayDanhSachNguoiDung_PhanTrang?MaNhom=${maNhom}&page=${currentPage}&pageSize=5`
      );
      dispatch(danhSachNguoiDungTheoTrang(data));
    } catch (error) {
      console.log(error);
    }
  };
};
export const LayDanhSachNguoiDungTheoTen = (currentPage = 1, event) => {
  return async (dispatch) => {
    const allParams = {
      MaNhom: maNhom,
      tuKhoa: event,
      page: currentPage,
      pageSize: 5,
    };

    try {
      const { data } = await Domain_Api.get(
        `/api/QuanLyNguoiDung/LayDanhSachNguoiDung_PhanTrang`,
        {
          params: Object.entries(allParams).reduce(
            (object, [k, v]) => (v ? ((object[k] = v), object) : object),
            {}
          ),
        }
      );
      dispatch(danhSachNguoiDungTheoTrang(data));
    } catch (error) {
      console.log(error);
    }
  };
};
export default QuanLyNguoiDungSlice.reducer;
//-----------------------------------------
