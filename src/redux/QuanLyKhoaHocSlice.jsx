import { maNhom } from "../Constants";
import { createSlice } from "@reduxjs/toolkit";
import { Domain_Api } from "../Services/config";
//---------------state-----------------
const initialState = {
  dsKHTheoDanhMuc: [],
  dsKHTheoTrang: [],
  infoCourse: {},
  paramsDangKyKhoaHoc: {},
  thongTinKhoaHocDeSua: {},
};
export const listMaNhomKhoaHoc = [
  {
    id: 1,
    maNhom: "GP01",
  },
  {
    id: 2,
    maNhom: "GP02",
  },
  {
    id: 3,
    maNhom: "GP04",
  },
  {
    id: 4,
    maNhom: "GP04",
  },
  {
    id: 5,
    maNhom: "GP05",
  },
  {
    id: 6,
    maNhom: "GP06",
  },
  {
    id: 7,
    maNhom: "GP07",
  },
  {
    id: 8,
    maNhom: "GP08",
  },
  {
    id: 9,
    maNhom: "GP09",
  },
  {
    id: 10,
    maNhom: "GP010",
  },
  {
    id: 11,
    maNhom: "GP011",
  },
  {
    id: 12,
    maNhom: "GP012",
  },
  {
    id: 13,
    maNhom: "GP013",
  },
  {
    id: 14,
    maNhom: "GP014",
  },
  {
    id: 15,
    maNhom: "GP015",
  },
];
const QuanLyKhoaHocSlice = createSlice({
  name: "QuanLyKhoaHocSlice",
  initialState,
  reducers: {
    khoaHocTheoDanhMuc: (state, { type, payload }) => {
      state.dsKHTheoDanhMuc = payload;
    },
    khoaHocTheoTrang: (state, { type, payload }) => {
      state.dsKHTheoTrang = payload;
    },
    thongTinKhoaHoc: (state, { type, payload }) => {
      state.infoCourse = payload;
    },
    dangKyKhoaHoc: (state, { type, payload }) => {
      state.paramsDangKyKhoaHoc = payload;
    },
    thongTinKhoaHocDeCapNhat: (state, { type, payload }) => {
      state.thongTinKhoaHocDeSua = payload;
    },
  },
});
export const {
  khoaHocTheoDanhMuc,
  khoaHocTheoTrang,
  thongTinKhoaHoc,
  dangKyKhoaHoc,
  thongTinKhoaHocDeCapNhat,
} = QuanLyKhoaHocSlice.actions;
export default QuanLyKhoaHocSlice.reducer;
//----------------------------------------

//-------------------api--------------------
export let LayDanhMucKhoaHoc = () => {
  return Domain_Api.get("/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc");
};
export let LayDanhSachKhoaHoc = () => {
  return Domain_Api.get(
    `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=${maNhom}`
  );
};
export let layKhoaHocDangKy = (params) => {
  return Domain_Api.post("/api/QuanLyKhoaHoc/DangKyKhoaHoc", params);
};
export let huyDangKyKhoaHoc = (params) => {
  return Domain_Api.post("/api/QuanLyKhoaHoc/HuyGhiDanh", params);
};
export let ThemKHUploadHinhAnh = (values) => {
  return Domain_Api.post("/api/QuanLyKhoaHoc/ThemKhoaHocUploadHinh", values);
};
export let SuaThongTinKhoaHoc = (values) => {
  return Domain_Api.put("/api/QuanLyKhoaHoc/CapNhatKhoaHoc", values);
};
export const LayKhoaHocTheoDanhMuc = (maDanhMuc) => {
  return async (dispatch) => {
    try {
      const { data } = await Domain_Api.get(
        `/api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc?maDanhMuc=${maDanhMuc}&MaNhom=${maNhom}`
      );
      dispatch(khoaHocTheoDanhMuc(data));
    } catch (error) {
      console.log(error);
    }
  };
};
export const LayKhoaHocTheoTrangUser = (currentPage) => {
  return async (dispatch) => {
    try {
      const { data } = await Domain_Api.get(
        `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc_PhanTrang?page=${currentPage}&pageSize=12&MaNhom=${maNhom}`
      );
      dispatch(khoaHocTheoTrang(data));
    } catch (error) {
      console.log(error);
    }
  };
};
export const LayKhoaHocTheoTrangAdmin = (currentPage = 1) => {
  return async (dispatch) => {
    try {
      const { data } = await Domain_Api.get(
        `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc_PhanTrang?page=${currentPage}&pageSize=5&MaNhom=${maNhom}`
      );
      dispatch(khoaHocTheoTrang(data));
    } catch (error) {
      console.log(error);
    }
  };
};
export const LayThongTinKhoaHoc = (maKhoaHoc) => {
  return async (dispatch) => {
    try {
      const { data } = await Domain_Api.get(
        `/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${maKhoaHoc}`
      );
      dispatch(thongTinKhoaHoc(data));
    } catch (error) {
      console.log(error);
    }
  };
};
export const LayDanhSachKhoaHocTheoTen = (currentPage = 1, event) => {
  return async (dispatch) => {
    const allParams = {
      MaNhom: maNhom,
      tenKhoaHoc: event,
      page: currentPage,
      pageSize: 5,
    };
    try {
      const { data } = await Domain_Api.get(
        `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc_PhanTrang`,
        {
          params: Object.entries(allParams).reduce(
            (object, [k, v]) => (v ? ((object[k] = v), object) : object),
            {}
          ),
        }
      );
      dispatch(khoaHocTheoTrang(data));
    } catch (error) {
      console.log(error);
    }
  };
};
export const LayDanhSachKhoaHocTheoTenUser = (currentPage = 1, event) => {
  return async (dispatch) => {
    const allParams = {
      MaNhom: maNhom,
      tenKhoaHoc: event,
      page: currentPage,
      pageSize: 12,
    };
    try {
      const { data } = await Domain_Api.get(
        `/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc_PhanTrang`,
        {
          params: Object.entries(allParams).reduce(
            (object, [k, v]) => (v ? ((object[k] = v), object) : object),
            {}
          ),
        }
      );
      dispatch(khoaHocTheoTrang(data));
    } catch (error) {
      console.log(error);
    }
  };
};
export const DeleteKhoaHoc = (maKhoaHoc) => {
  return Domain_Api.delete(
    `api/QuanLyKhoaHoc/XoaKhoaHoc?maKhoaHoc=${maKhoaHoc}`
  );
};
//--------------------------------------------
