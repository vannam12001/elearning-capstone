import React from "react";
import Lottie from "lottie-react";
import student from "./student.json";
function Review() {
  const cssInfoCourse = `
    container xl:flex xl:justify-center xl:items-center 
    lg:flex lg:justify-center lg:items-center 
    md:flex md:justify-center md:items-center
    sm:block
    min-[360px]:block
  `;
  const cssItemLeft = `
    sm:h-[400px] sm:w-[400px]
   `;
  return (
    <div className="py-14">
      <div className={cssInfoCourse}>
        <div className="w-1/2 sm:w-full min-[360px]:w-full">
          <Lottie
            animationData={student}
            className={cssItemLeft}
            style={{ margin: "auto" }}
          />
        </div>
        <div className="w-1/2 sm:w-full min-[360px]:w-full min-[360px]:pt-3">
          <div>
            <p className="xl:text-3xl lg:text-3xl md:text-2xl sm:text-2xl min-[360px]:text-2xl font-medium text-green-600 pb-4">
              Chương trình giảng dạy được biên soạn dành riêng cho các bạn Lập
              trình từ trái ngành hoặc đã có kiến thức theo cường độ cao, luôn
              được tinh chỉnh và tối ưu hóa theo thời gian bởi các thành viên
              sáng lập và giảng viên dày kinh nghiệm. Thực sự rất hay và hấp
              dẫn.
            </p>
            <p className="xl:text-2xl lg:text-2xl md:text-xl sm:text-xl min-[360px]:text-xl pb-4 font-medium">
              Trích lời của thầy (lương){" "}
              <span className="color-changing-text font-medium xl:text-3xl lg:text-3xl md:text-2xl sm:text-xl min-[360px]:text-xl">
                Trần Quang Sĩ
              </span>
              .
            </p>
            <p className="xl:text-2xl lg:text-2xl md:text-xl sm:text-xl min-[360px]:text-xl font-medium">
              Tụi Em Cảm Ơn Thầy Và Các Mentor Nhiều Ạ. Mặc Dù Thầy Hơi Lương.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Review;
