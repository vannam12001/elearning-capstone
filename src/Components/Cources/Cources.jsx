import React, { useEffect, useState } from "react";
import { LayDanhSachKhoaHoc } from "../../redux/QuanLyKhoaHocSlice";
import { Typography, Avatar } from "antd";
import { StarFilled } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
const { Paragraph, Text } = Typography;
const Cources = () => {
  const navigate = useNavigate();
  const [khoaHoc, setKhoaHoc] = useState([]);
  const fetch = async () => {
    const result = await LayDanhSachKhoaHoc();
    setKhoaHoc(result.data);
  };
  useEffect(() => {
    fetch();
  }, []);
  const products = khoaHoc.slice(0, 12).map((item, index) => {
    return {
      id: index + 1,
      maKhoaHoc: item.maKhoaHoc,
      biDanh: item.biDanh,
      tenKhoaHoc: item.tenKhoaHoc,
      moTa: item.moTa,
      luotXem: item.luotXem,
      hinhAnh:
        "https://www.altamira.ai/wp-content/uploads/2022/12/Full-Stack-DeveloperArtboard-2.png",
      ngayTao: item.ngayTao,
      nguoiTao: {
        hoTen: item.nguoiTao.hoTen,
      },
    };
  });
  return (
    <div className="container py-14">
      <h1 className="text-yellow-600 text-3xl font-medium pb-5">
        DANH SÁCH KHÓA HỌC
      </h1>
      <div>
        <div className="bg-white">
          <div className="mx-auto max-w-2xl sm:px-6 sm:py-15 lg:py-10 lg:max-w-7xl">
            <div className="grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 lg:grid-cols-4 xl:gap-x-8">
              {products.map((product) => (
                <div
                  key={product.id}
                  onClick={() => {
                    navigate(`/thongTinKhoaHoc/${product.maKhoaHoc}`);
                  }}
                  className="group relative border shadow-md cursor-pointer transition delay-200 rounded-lg hover:-translate-y-2"
                >
                  <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none lg:h-64">
                    <img
                      src={product.hinhAnh}
                      alt=""
                      className="w-full h-full object-cover object-center lg:h-full lg:w-full"
                    />
                  </div>
                  <div className="p-4">
                    <Text
                      className="text-yellow-600 py-3 lg:text-2xl sm:text-2xl min-[360px]:text-4xl font-medium"
                      style={{
                        width: 50,
                      }}
                    >
                      {product.tenKhoaHoc}
                    </Text>
                    <Paragraph
                      className="text-gray-500 lg:text-xl sm:text-xl min-[360px]:text-3xl"
                      ellipsis={{
                        rows: 2,
                      }}
                    >
                      {product.moTa}
                    </Paragraph>
                    <div className="flex pb-5">
                      <div className="xl:w-1/5 lg:w-1/5">
                        <Avatar
                          src="https://i.pravatar.cc/200?u=fake@pravatar.com"
                          className="lg:h-10 lg:w-10 min-[360px]:h-14 min-[360px]:w-14"
                        ></Avatar>
                      </div>
                      <div className="xl:w-4/5 lg:w-4/5 xl:p-2 lg:py-1 lg:pl-2 min-[360px]:p-3 justify-center">
                        <p className="uppercase font-medium lg:text-xl sm:text-xl min-[360px]:text-2xl">
                          {product.nguoiTao.hoTen}
                        </p>
                      </div>
                    </div>
                    <hr />
                    <div className="flex items-center">
                      <div className="xl:w-[10%] lg:w-[10%] lg:pt-2 sm:w-[10%] min-[360px]:w-1/2">
                        <h1 className="color-changing-text xl:text-3xl lg:text-2xl md:text-2xl sm:text-2xl min-[360px]:text-3xl">
                          FREE
                        </h1>
                      </div>
                      <div className="xl:w-[90%] lg:w-[90%] text-right sm:w-[90%] sm:pl-4 min-[360px]:w-1/2">
                        <h1>
                          <StarFilled className="text-yellow-400 xl:text-3xl lg:text-3xl md:text-2xl sm:text-2xl min-[360px]:text-3xl" />{" "}
                          (4.5)
                        </h1>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cources;
