import React from "react";
import { useSelector } from "react-redux";
import Lottie from "lottie-react";
import animate_loading from "./animation_loading.json";
function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerSlice);
  return isLoading ? (
    <div className="h-screen w-screen bg-gray-400 fixed top-0 left-0 z-30 flex justify-center items-center">
      <Lottie
        animationData={animate_loading}
        loop={true}
        width="50%"
        height="50%"
      />
    </div>
  ) : (
    <></>
  );
}

export default Spinner;
