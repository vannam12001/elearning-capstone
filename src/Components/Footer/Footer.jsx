import React from "react";
import { PhoneFilled, MailFilled } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { Form, Input, message } from "antd";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const Footer = () => {
  const listMaDanhMuc = [
    { maDanhMuc: "BackEnd", tenDanhMuc: "Lập trình Backend" },
    { maDanhMuc: "Design", tenDanhMuc: "Thiết kế Web" },
    { maDanhMuc: "DiDong", tenDanhMuc: "Lập trình di động" },
    { maDanhMuc: "FrontEnd", tenDanhMuc: "Lập trình Front end" },
    { maDanhMuc: "FullStack", tenDanhMuc: "Lập trình Full Stack" },
    { maDanhMuc: "TuDuy", tenDanhMuc: "Tư duy lập trình" },
  ];
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log("Success:", values);
    toast(
      <div>
        <h1 className="py-4">
          Chúng Tôi Đã Nhận Được Thông Tin Liên Hệ Của Bạn:
        </h1>
        <ul>
          <li>Họ Và Tên: {values.hoTen}</li>
          <li>Email: {values.email}</li>
          <li>Số Điện Thoại: {values.soDt}</li>
        </ul>
        <h1 className="py-4">
          Chúng Tôi Sẽ Liên Hệ Với Bạn Sớm Nhất!!! Xin Cảm Ơn Và Hẹn Gặp Lại!!!
        </h1>
      </div>
    );
  };

  return (
    <div className="bg-blue-100">
      <div
        className="
      p-10 grid xl:grid-cols-3 
      lg:grid-cols-3 md:grid-cols-3 
      sm:grid-cols-2 sm:gap-3 
      min-[360px]:grid-cols-1 min-[360px]:gap-3"
      >
        <div>
          <h1
            className="
            xl:text-3xl lg:text-3xl 
            md:text-2xl sm:text-xl min-[360px]:text-2xl
            text-yellow-600 font-bold cursor-pointer"
            onClick={() => {
              navigate("/");
            }}
          >
            CYPERLEARNING
          </h1>
          <h1 className="py-2">
            <PhoneFilled className="text-3xl text-green-600" />
            <span
              className="
            pl-2 xl:text-xl 
            lg:text-xl md:text-lg 
            sm:text-lg cursor-pointer 
            font-medium delay-200 min-[360px]:text-xl 
            transition hover:text-green-600"
            >
              0326034561
            </span>
          </h1>
          <h1 className="py-2">
            <MailFilled className="text-3xl text-green-600" />
            <span
              className="
            pl-2 xl:text-xl 
            lg:text-xl md:text-lg 
            sm:text-lg cursor-pointer 
            font-medium delay-200 min-[360px]:text-xl 
            transition hover:text-green-600"
            >
              abc@gmail.com
            </span>
          </h1>
          <h1 className="py-2">
            <i className="fas fa-map-marker-alt text-3xl text-green-600"></i>
            <span
              className="
            pl-5 xl:text-xl 
            lg:text-xl md:text-lg 
            sm:text-lg cursor-pointer 
            font-medium delay-200 min-[360px]:text-xl 
            transition hover:text-green-600"
            >
              Hồ Chí Minh
            </span>
          </h1>
        </div>
        <div className="grid grid-cols-2">
          <div>
            <h1
              className="
            xl:text-3xl lg:text-3xl 
            md:text-2xl sm:text-xl 
            font-medium text-yellow-600 
            min-[360px]:text-2xl cursor-pointer"
            >
              LIÊN KẾT
            </h1>
            <h1
              className="
              xl:text-xl lg:text-xl 
              md:text-lg sm:text-lg 
              py-2 font-medium min-[360px]:text-xl 
              cursor-pointer transition 
              delay-200 hover:text-green-600"
              onClick={() => {
                navigate("/");
              }}
            >
              Trang Chủ
            </h1>
            <h1
              className="
            xl:text-xl lg:text-xl 
            md:text-lg sm:text-lg 
            py-2 font-medium min-[360px]:text-xl 
            cursor-pointer transition 
            delay-200 hover:text-green-600"
            >
              Danh Mục
            </h1>
            <h1
              className="
              xl:text-xl lg:text-xl 
              md:text-lg sm:text-lg 
              py-2 font-medium min-[360px]:text-xl 
              cursor-pointer transition 
              delay-200 hover:text-green-600"
              onClick={() => {
                navigate("/khoaHoc");
              }}
            >
              Khóa Học
            </h1>
            <h1
              className="
              xl:text-xl lg:text-xl 
              md:text-lg sm:text-lg 
              py-2 font-medium min-[360px]:text-xl 
              cursor-pointer transition 
              delay-200 hover:text-green-600"
              onClick={() => {
                navigate("/thongTin");
              }}
            >
              Thông Tin
            </h1>
          </div>
          <div>
            <h1
              className="
            xl:text-3xl lg:text-3xl 
            md:text-2xl sm:text-xl 
            font-medium text-yellow-600 
            min-[360px]:text-2xl cursor-pointer"
            >
              KHÓA HỌC
            </h1>
            {listMaDanhMuc.map((item, index) => {
              return (
                <h1
                  key={index}
                  className="
                  xl:text-xl lg:text-xl 
                  md:text-lg sm:text-lg 
                  py-2 font-medium min-[360px]:text-xl 
                  cursor-pointer transition 
                  delay-200 hover:text-green-600"
                  onClick={() => {
                    navigate(`/danhMucKhoaHoc/${item.maDanhMuc}`);
                  }}
                >
                  {item.tenDanhMuc}
                </h1>
              );
            })}
          </div>
        </div>
        <div>
          <h1
            className="
          xl:text-3xl lg:text-3xl 
          md:text-2xl sm:text-2xl 
          font-medium text-yellow-600 
          min-[360px]:text-2xl cursor-pointer pb-2"
          >
            ĐĂNG KÝ TƯ VẤN
          </h1>
          <Form form={form} onFinish={onFinish} name="tuVan">
            <Form.Item
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập họ và tên!!!",
                },
              ]}
            >
              <Input
                placeholder="Họ Và Tên"
                className="border-2 border-green-600"
              />
            </Form.Item>
            <Form.Item
              name="email"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập email!!!",
                },
                {
                  type: "email",
                  message: "Vui lòng nhập đúng email!!!",
                },
              ]}
            >
              <Input
                placeholder="Email"
                className="border-2 border-green-600"
              />
            </Form.Item>
            <Form.Item
              name="soDt"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập số điện thoại!!!",
                },
                {
                  pattern: /^(0\d{9}|84\d{9})$/,
                  message: "Vui lòng nhập đúng số điện thoại!!!",
                },
              ]}
            >
              <Input
                placeholder="Số Điện Thoại"
                className="border-2 border-green-600"
              />
            </Form.Item>
            <Form.Item>
              <button className="bg-yellow-600 font-medium transition delay-200 hover:text-white px-3 py-2 text-xl rounded-lg">
                Đăng Ký
              </button>
            </Form.Item>
          </Form>
          <ToastContainer />
        </div>
      </div>
      <div
        className="
      mx-5 p-5 border-t border-gray-500
      xl:grid xl:grid-cols-2
      lg:grid lg:grid-cols-2
      md:grid md:grid-cols-2
      sm:block min-[360px]:block"
      >
        <div>
          <h1 className="xl:text-xl lg:text-xl md:text-xl sm:text-xl min-[360px]:text-lg">
            Copyright © 2021. All rights reserved.
          </h1>
        </div>
        <div className="xl:text-right lg:text-right md:text-right sm:pt-3 min-[360px]:pt-3">
          <h1>
            <span className="pr-3">
              <i className="fab fa-instagram text-3xl text-green-600"></i>
            </span>
            <span className="px-3">
              <i className="fab fa-facebook-square text-3xl text-green-600"></i>
            </span>
            <span className="pl-3">
              <i className="fab fa-tiktok text-3xl text-green-600"></i>
            </span>
          </h1>
        </div>
      </div>
    </div>
  );
};

export default Footer;
