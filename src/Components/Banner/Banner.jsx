import React from "react";
import Lottie from "lottie-react";
import bg_banner from "./bg_banner.json";
import "../../index.css";
const Banner = () => {
  const cssBanner = `
  container grid grid-cols-1 gap-x-6 gap-y-10 sm:grid-cols-2 
  xl:grid-cols-2 lg:grid-cols-2 min-[360px]:grid-cols-1 
  md:grid-cols-2 xl:gap-x-8
  `;
  const cssItemLeft = `
  xl:h-screen xl:flex xl:justify-center xl:items-center 
  lg:h-screen lg:flex lg:justify-center lg:items-center 
  md:h-screen md:flex md:justify-center md:items-center md:pl-5 
  sm:h-screen sm:flex sm:justify-center sm:items-center 
  min-[360px]:pt-[150px] min-[360px]:flex min-[360px]:justify-center min-[360px]:items-center
  relative z-10 `;
  const cssItemRight =
    "xl:h-screen lg:flex lg:items-center md:flex md:items-center sm:flex sm:items-center";
  const cssAnimation =
    "animated-text lg:text-4xl md:text-3xl sm:text-2xl min-[360px]:text-2xl text-green-600";
  const cssBtn =
    "mt-5 text-2xl bg-yellow-600 py-4 px-3 delay-200 transition rounded hover:text-white";
  return (
    <div className={cssBanner}>
      <div className={cssItemLeft}>
        <div>
          <h1 className="lg:text-5xl md:text-3xl sm:text-2xl min-[360px]:text-3xl font-bold text-center">
            CHÀO MỪNG{" "}
            <span className="lg:hidden min-[360px]:block">
              ĐẾN VỚI MÔI TRƯỜNG
            </span>
          </h1>
          <h1 className="lg:text-5xl min-[360px]:text-4xl font-bold text-center">
            <span className="min-[360px]:hidden lg:block">
              ĐẾN VỚI MÔI TRƯỜNG
            </span>
          </h1>
          <h1 className={cssAnimation}>CYPERCAPSTONE LEARNING</h1>
          <div className="lg:text-left min-[360px]:text-center">
            <button className={cssBtn}>BẮT ĐẦU NÀO</button>
          </div>
        </div>
      </div>
      <div className={cssItemRight}>
        <Lottie
          animationData={bg_banner}
          className="justify-center"
          loop={true}
          width="100%"
          height="100%"
        />
      </div>
    </div>
  );
};

export default Banner;
