import axios from "axios";
import {
  CYPERLEARNING_API,
  CYPERLEARN_LOCALSTORAGE,
  TOKEN_CYPERSOFT,
} from "../Constants/index";
import { localStorageServices } from "./localStorageServices";
import { store } from "../main";
import { batLoading, tatLoading } from "../redux/spinnerSlice";

export let Domain_Api = axios.create({
  baseURL: CYPERLEARNING_API,
  // headers: {
  //   TokenCybersoft: TOKEN_CYPERSOFT,
  //   Authorization: `Bearer ${
  //     localStorageServices.getUser(CYPERLEARN_LOCALSTORAGE)?.accessToken
  //   }`,

  // },
});
Domain_Api.interceptors.request.use(
  function (config) {
    store.dispatch(batLoading());
    config.headers.TokenCybersoft = TOKEN_CYPERSOFT;
    config.headers.Authorization = `Bearer ${
      localStorageServices.getUser(CYPERLEARN_LOCALSTORAGE)?.accessToken
    }`;
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

Domain_Api.interceptors.response.use(
  function (response) {
    setTimeout(() => {
      store.dispatch(tatLoading());
    }, 1000);
    return response;
  },
  function (error) {
    store.dispatch(tatLoading());
    return Promise.reject(error);
  }
);
